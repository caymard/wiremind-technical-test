from fastapi import FastAPI

app = FastAPI()


@app.get("/")
def read_item(firstname: str = "", lastname: str = ""):
    return {"message": "Hello World", "firstname": firstname, "lastname": lastname}
